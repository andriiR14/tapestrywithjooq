package org.andrii.services;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    private DSLContext dsl;

    @Autowired
    public TestService(DSLContext dsl) {
        this.dsl = dsl;
    }
}
