package org.andrii.jooq;

import org.andrii.hello.db.adb.tables.Menu;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MenuJooq {

    //@Inject
    @Autowired
    private DSLContext dsl;

    public Collection<String> getMenuItems() {

        return dsl.selectFrom(Menu.MENU).fetch(Menu.MENU.NAME);
    }
    
}
